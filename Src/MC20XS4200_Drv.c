/*
 * MC20XS4200_Drv.c
 *
 *  Created on: 24 juin 2021
 *      Author: STuser
 */


#include "MC20XS4200_Drv.h"
#include "dspi1.h"
uint16_t tu16XS4200Channel[2];
uint16_t watchDog = 0;
#define BUFFER_SIZE 7U
uint16_t TXbuffer[BUFFER_SIZE] = {0};
uint16_t RXbuffer[BUFFER_SIZE] = {0};
uint16_t TXcounter = 0;
uint16_t u16DrvDes_MC20XS4200_Config(XS_SpdInRegsDataPtr XS_SpdInRegsData)
{
	uint16_t status = 1U;
	uint16_t channel;

	TXcounter = 0;
	/**************************************
	 * XS4200 Init Sequence GCR register  *
	 **************************************/
	XS_SpdInRegsData[0].XS_GcrIn.All 				= 0x0000;				// Clear register, Default value of GCR
	XS_SpdInRegsData[0].XS_GcrIn.u3RAddr 			= XS_SPD_GCRR;
	XS_SpdInRegsData[0].XS_GcrIn.u1OV 				= 0U;					//Enable over-voltage protection
	XS_SpdInRegsData[0].XS_GcrIn.u1VDDFAILen 		= 1U;					//Enable VDD Fail (Test)
	XS_SpdInRegsData[0].XS_GcrIn.u1WDdis 			= 1U;					//Disable Watch-dog in Debug Mode
	XS_SpdInRegsData[0].XS_GcrIn.u1THen 			= 1U;					//Enable Track & Hold
	XS_SpdInRegsData[0].XS_GcrIn.u1PARALLEL 		= 0U;					//Disable Parallel
	XS_SpdInRegsData[0].XS_GcrIn.u1PWM0en 			= 1U;					//Enable PWM0
	XS_SpdInRegsData[0].XS_GcrIn.u1PWM1en 			= 1U;					//Enable PWM1
	XS_SpdInRegsData[0].XS_GcrIn.u1A0 				= 0;					//Channel Selection you must selection for channel 0 and 1
	XS_SpdInRegsData[0].XS_GcrIn.u1WDIN 			= watchDog;				//Watchdog Toggle
	XS_SpdInRegsData[0].XS_GcrIn.u1P 				= u16Drvdes_XS_iParityCalc(XS_SpdInRegsData[0].XS_GcrIn.All);//Cache GCR Word before parity calculation

	TXbuffer[TXcounter] = XS_SpdInRegsData[TXcounter].XS_GcrIn.All;
	watchDog ^= 1;
	TXcounter++;

	/*************************************
	 * XS4200 Init Sequence OCR register *
	 *************************************/
	for (channel = 0; channel < XS_MAX_HS; channel++)
	{

		XS_SpdInRegsData[channel].XS_OcrIn.All 			= 0x0000U;						// Clear OCr register
		XS_SpdInRegsData[channel].XS_OcrIn.u1OCL 		= 1U;							//OCL3 Selection
		XS_SpdInRegsData[channel].XS_OcrIn.u1OCM 		= 1U;
		XS_SpdInRegsData[channel].XS_OcrIn.u1HOCR 		= 1U;
		XS_SpdInRegsData[channel].XS_OcrIn.u1tOCM 		= 0U;							//OCM2 Selection
		XS_SpdInRegsData[channel].XS_OcrIn.u1tOCH 		= 0U;							//OCH2 Selection
		XS_SpdInRegsData[channel].XS_OcrIn.u1CSNSrat 	= 1U;							//Low current mode 10 mOhms ,  ratio = 1/3000
		XS_SpdInRegsData[channel].XS_OcrIn.u1CLOCKsel 	= 1U;
		XS_SpdInRegsData[channel].XS_OcrIn.u1PR 		= 0U;
		XS_SpdInRegsData[channel].XS_OcrIn.u1HOCR 		= 1U;							//OCL3 Selection
		XS_SpdInRegsData[channel].XS_OcrIn.u3RAddr 		= XS_SPD_OCRR;
		XS_SpdInRegsData[channel].XS_OcrIn.u1A0 		= channel;
		XS_SpdInRegsData[channel].XS_OcrIn.u1WDIN	    = watchDog;
		XS_SpdInRegsData[channel].XS_OcrIn.u1P 			= u16Drvdes_XS_iParityCalc(XS_SpdInRegsData[channel].XS_OcrIn.All);

		TXbuffer[TXcounter] = XS_SpdInRegsData[channel].XS_OcrIn.All;
		watchDog ^=1;
		TXcounter++;
	}
	/*************************************
	 * XS4200 Init Sequence CONF register*
	 *************************************/
	for (channel = 0; channel < XS_MAX_HS; channel++)
	{
		XS_SpdInRegsData[channel].XS_ConfIn.All 		= 0x0000U;
		XS_SpdInRegsData[channel].XS_ConfIn.u3RAddr 	= XS_SPD_CONFR;
		XS_SpdInRegsData[channel].XS_ConfIn.u3DELAY 	= 0U;
		XS_SpdInRegsData[channel].XS_ConfIn.u2SR 		= 2U;							//Maximum slew rate
		XS_SpdInRegsData[channel].XS_ConfIn.u1DIR 		= 0;
		XS_SpdInRegsData[channel].XS_ConfIn.u1OLOFF 	= 1U;							//Disable OLOFF by Default
		XS_SpdInRegsData[channel].XS_ConfIn.u1OLON 		= 0U;
		XS_SpdInRegsData[channel].XS_ConfIn.u1OS 		= 0U;
		XS_SpdInRegsData[channel].XS_ConfIn.u1A0 		= channel;
		XS_SpdInRegsData[channel].XS_ConfIn.u1WDIN	    = watchDog;
		XS_SpdInRegsData[channel].XS_ConfIn.u1P 		= u16Drvdes_XS_iParityCalc(XS_SpdInRegsData[channel].XS_ConfIn.All);

		TXbuffer[TXcounter] = XS_SpdInRegsData[channel].XS_ConfIn.All;
		watchDog ^=1;
		TXcounter++;
	}

	/*******************************************
	 * XS4200 Init Sequence AUTORETRY register *
	 *******************************************/
	for (channel = 0; channel < XS_MAX_HS; channel++)
	{
		XS_SpdInRegsData[channel].XS_RetrayIn.All 			= 0x0000U;
		XS_SpdInRegsData[channel].XS_RetrayIn.u3RAddr 		= XS_SPD_RETRYR;
		XS_SpdInRegsData[channel].XS_RetrayIn.u1Retry_s 	= 0U;										// Enable Autoretry for Bulb load (default value)
		XS_SpdInRegsData[channel].XS_RetrayIn.u1Retryinlim 	= 0U;										// Disable Unlimited Autoretry
		XS_SpdInRegsData[channel].XS_RetrayIn.u2Autoperiod 	= 3U;										// Minimal Auto Retry period 13.1ms
		XS_SpdInRegsData[channel].XS_RetrayIn.u1Confspi 	= 0U;										// CONF_SPI_s default value
		XS_SpdInRegsData[channel].XS_RetrayIn.u1A0 			= channel;
		XS_SpdInRegsData[channel].XS_RetrayIn.u1WDIN	    = watchDog;
		XS_SpdInRegsData[channel].XS_RetrayIn.u1P 			= u16Drvdes_XS_iParityCalc(XS_SpdInRegsData[channel].XS_RetrayIn.All);

		TXbuffer[TXcounter] = XS_SpdInRegsData[channel].XS_RetrayIn.All;
		watchDog ^=1;
		TXcounter++;
	}

	/* send the buffer */
	DSPI_MasterTransfer(INST_DSPI1, TXbuffer, RXbuffer, BUFFER_SIZE);
	return status;
}

/* update pwm */
uint16_t u16Drvdes_MC20XS4200_PwmUpdate(XS_SpdInRegsDataPtr XS_SpdInRegsData, uint16_t duty)
{
	uint16_t status;
	uint16_t channel;
	uint16_t *SendPtr;
	uint16_t *ReceivePtr;
	uint16_t des_status;

	for (channel = 0; channel < XS_MAX_HS; channel++)
	{
		XS_SpdInRegsData[channel].XS_PwmIn.u3RAddr 		 = XS_SPD_PWMR;
		XS_SpdInRegsData[channel].XS_PwmIn.u8PWM		 = (uint8_t)0x00ff&duty;
		XS_SpdInRegsData[channel].XS_PwmIn.u1A0 		 = channel;
		XS_SpdInRegsData[channel].XS_PwmIn.u1WDIN	     = watchDog;
		XS_SpdInRegsData[channel].XS_PwmIn.u1P 			 = u16Drvdes_XS_iParityCalc((uint16_t)XS_SpdInRegsData[channel].XS_PwmIn.All);

		SendPtr = (uint16_t*)&XS_SpdInRegsData[channel].XS_PwmIn.All;
		des_status = DSPI_MasterTransfer(INST_DSPI1, SendPtr, ReceivePtr, 1);

		watchDog ^=1;

	}

	return status;
}
void des_MC20XS4200_Task(void)
{
	/* write data to buffer OCR START RETRAY and GCR */

	/* chose the channel */

	/* write reg address */

	/* toggle watchdog */

	/* calculate parity */

	/* write the txbuffer */
}


uint16_t u16Drvdes_XS_iParityCalc (uint16_t Fu16Word)
{
    /* use a technique found on internet to process the parity bit */
    /* https://www.geeksforgeeks.org/finding-the-parity-of-a-number-efficiently/ */
    Fu16Word = Fu16Word ^ (Fu16Word >> 8);
    Fu16Word = Fu16Word ^ (Fu16Word >> 4);
    Fu16Word = Fu16Word ^ (Fu16Word >> 2);
    Fu16Word = Fu16Word ^ (Fu16Word >> 1);

    return (Fu16Word & 1);
}

