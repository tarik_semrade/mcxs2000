/*
 * MC20XS4200_Drv.h
 *
 *  Created on: 24 juin 2021
 *      Author: STuser
 */

#ifndef INC_MC20XS4200_DRV_H_
#define INC_MC20XS4200_DRV_H_
#include <stdint.h>

/* MC20XS4200 default configuration */
#define XS20_PWMR_0_INIT        XS_OFF|0x7F
#define XS20_PWMR_1_INIT        XS_OFF|0x7F

#define XS20_CONFR0_0_INIT      XS_DIR_DIS|XS_SLEW_LOW|XS_NO_DELAY
#define XS20_CONFR0_1_INIT      XS_DIR_DIS|XS_SLEW_LOW|XS_NO_DELAY

#define XS20_CONFR1_0_INIT      XS_RETRY_DIS|XS_OS_DIS|XS_OLON_EN|XS_OLOFF_DIS|XS_OLLED_DIS|XS_CSNS_LOW
#define XS20_CONFR1_1_INIT      XS_RETRY_DIS|XS_OS_DIS|XS_OLON_EN|XS_OLOFF_DIS|XS_OLLED_DIS|XS_CSNS_LOW

#define XS20_OCR_0_XENINIT      XS_XENON_ENABLED
#define XS20_OCR_1_XENINIT      XS_XENON_ENABLED

#define XS20_OCR_0_INIT         XS_INRUSH_SLOW|XS_OC_INRUSH_ONLY
#define XS20_OCR_1_INIT         XS_INRUSH_SLOW|XS_OC_INRUSH_ONLY

#define XS20_GCR_INIT           XS_PWM_INTCLK|XS_TEMP_DIS|XS_CSNS_DIS|XS_UV_EN|XS_OV_EN

/* Operation modes */
#define XS_SLEEP                0
#define XS_NORMAL               1

/* Registers */
#define XS_STATR                0x00
#define XS_PWMR                 0x04
#define XS_CONFR0               0x08
#define XS_CONFR1               0x0C
#define XS_OCR                  0x10
#define XS_GCR                  0x14
#define XS_CALR                 0x1D

/* Channel Selection */
#define XS_HS0                  0x00
#define XS_HS1                  0x20
#define XS_MAX_HS				2U

/* PWM Register */
#define XS_ON                   0x80
#define XS_OFF                  0x00

/* CONFR0 Register */
#define XS_DIR_EN               0x00
#define XS_DIR_DIS              0x20
#define XS_SLEW_MED             0x00
#define XS_SLEW_LOW             0x08
#define XS_SLEW_HIGH            0x10
#define XS_NO_DELAY             0x00
#define XS_DELAY_16             0x01
#define XS_DELAY_32             0x02
#define XS_DELAY_48             0x03
#define XS_DELAY_64             0x04
#define XS_DELAY_80             0x05
#define XS_DELAY_96             0x06
#define XS_DELAY_112            0x07

/* CONFR1 Register */
#define XS_RETRY_UNL            0x40
#define XS_RETRY_DIS            0x20
#define XS_OS_DIS               0x10
#define XS_OLON_EN              0x00
#define XS_OLON_DIS             0x08
#define XS_OLOFF_EN             0x00
#define XS_OLOFF_DIS            0x04
#define XS_OLLED_EN             0x02
#define XS_OLLED_DIS            0x00
#define XS_CSNS_LOW             0x00
#define XS_CSNS_HIGH            0x01

/* Open load selection */
#define XS_NO_LOAD              XS_OLON_DIS
#define XS_LED                  XS_OLLED_EN
#define XS_BULB                 XS_OLLED_DIS

/* Overcurrent Register */
#define XS_XENON_ENABLED        0x00
#define XS_XENON_DISABLED       0x01
#define XS_BULB_COOL_MED        0x00
#define XS_BULB_COOL_LOW        0x40
#define XS_BULB_COOL_HIGH       0x80
#define XS_INRUSH_SLOW          0x00
#define XS_INRUSH_FAST          0x10
#define XS_INRUSH_MEDIUM        0x20
#define XS_INRUSH_VERY_SLOW     0x30
#define XS_OCH1_TO_OCH2         0x08
#define XS_STEADY_OCL1          0x06
#define XS_STEADY_OCL2          0x00
#define XS_STEADY_OCL3          0x02
#define XS_STEADY_OCL4          0x04
#define XS_OC_INRUSH_ONLY       0x00
#define XS_OC_INRUSH_COOLING    0x01

/* Global Configuration Register */
#define XS_PWM_DISABLED         0x00
#define XS_PWM_IN0CLK           0x80
#define XS_PWM_INTCLK           0xC0
#define XS_TEMP_EN              0x20
#define XS_TEMP_DIS             0x00
#define XS_CSNS_EN              0x10
#define XS_CSNS_DIS             0x00
#define XS_CSNS_HS0             0x00
#define XS_CSNS_HS1             0x04
#define XS_CSNS_HS2             0x08
#define XS_CSNS_HS3             0x0C
#define XS_UV_EN                0x02
#define XS_OV_EN                0x00

/* SOA bits for reading through STATR */
#define XS_HS0_RD               0x00
#define XS_HS1_RD               0x08
#define XS_HS2_RD               0x10
#define XS_HS3_RD               0x18
#define XS_FLTR_RD              0x00
#define XS_PWMR_RD              0x01
#define XS_CONFR0_RD            0x02
#define XS_CONFR1_RD            0x03
#define XS_OCR_RD               0x04
#define XS_GCR_RD               0x05
#define XS_DIAGR0               0x07
#define XS_DIAGR1               0x0F
#define XS_DIAGR2               0x17

/* Fault Register flags */
#define XS_FAULT_UVF            0x80
#define XS_FAULT_OVF            0x40
#define XS_FAULT_OLON           0x20
#define XS_FAULT_OLOFF          0x10
#define XS_FAULT_OSF            0x08
#define XS_FAULT_OTF            0x04
#define XS_FAULT_SCF            0x02
#define XS_FAULT_OCF            0x01

/* SPI Command */
#define  XS_SPD_STATR               0x00
#define  XS_SPD_PWMR                0x01
#define  XS_SPD_CONFR               0x02
#define  XS_SPD_OCRR                0x04
#define  XS_SPD_RETRYR              0x05
#define  XS_SPD_GCRR                0x06
#define  XS_SPD_CALR                0x07

/* Registers Addr */
#define  XS_SPD_STATR_ADDR           0x00
#define  XS_SPD_FAULTR_ADDR          0x01
#define  XS_SPD_PWMR_ADDR            0x02
#define  XS_SPD_CONFR_ADDR           0x03
#define  XS_SPD_OCR_ADDR             0x04
#define  XS_SPD_RETRYR_ADDR          0x05
#define  XS_SPD_GCR_ADDR             0x06
#define  XS_SPD_DIAGR_ADDR           0x07

/* Magic Word for Calibration */
#define  Du16IO_iSPDCalibMagicNb       0x15B

/* Input Registers Description ******************************************/
extern uint32_t Spd_TX_Buffer[5];

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :7;
		uint16_t u3SOA :3;
	};
} Tst_eXS_STATR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :1;
		uint16_t u1ON :1;
		uint16_t u8PWM :8;
	};
} Tst_eXS_PWMR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :1;
		uint16_t u1OS :1;
		uint16_t u1OLON :1;
		uint16_t u1OLOFF :1;
		uint16_t u1DIR :1;
		uint16_t u2SR :2;
		uint16_t u3DELAY :3;
	};
} Tst_eXS_CONFR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :1;
		uint16_t u1HOCR :1;
		uint16_t u1PR :1;
		uint16_t u1CLOCKsel :1;
		uint16_t u1CSNSrat :1;
		uint16_t u1tOCH :1;
		uint16_t u1tOCM :1;
		uint16_t u1OCH :1;
		uint16_t u1OCM :1;
		uint16_t u1OCL :1;
	};
} Tst_eXS_OCR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :5;
		uint16_t u1Confspi :1;
		uint16_t u2Autoperiod :2;
		uint16_t u1Retryinlim :1;
		uint16_t u1Retry_s :1;
	};
} Tst_eXS_RETRYR_In_Bits;


typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :1;
		uint16_t u1PWM1en :1;
		uint16_t u1PWM0en :1;
		uint16_t u1PARALLEL :1;
		uint16_t u1THen :1;
		uint16_t u1WDdis :1;
		uint16_t u1VDDFAILen :1;
		uint16_t u1CSNS1en :1;
		uint16_t u1CSNS0en :1;
		uint16_t u1OV :1;
	};

} Tst_eXS_GCR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :1;
		uint16_t u9Word :9;
	};
} Tst_eXS_CALR_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1P :1;
		uint16_t u1A0 :1;
		uint16_t u3RAddr :3;
		uint16_t :10;
	};
} Tst_eXS_Header_In_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1OV :1;
		uint16_t u1UV :1;
		uint16_t u1POR :1;
		uint16_t u1RFULL1 :1;
		uint16_t u1RFULL0 :1;
		uint16_t u1FAULT1 :1;
		uint16_t u1FAULT0 :1;
		uint16_t u1OUT1 :1;
		uint16_t u1OUT0 :1;
	};
} Tst_eXS_STATR_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1OTW :1;
		uint16_t :2;
		uint16_t u1OLON :1;
		uint16_t u1OLOFF :1;
		uint16_t u1OS :1;
		uint16_t u1OT :1;
		uint16_t u1SC :1;
		uint16_t u1OC :1;
	};

} Tst_eXS_eFAULT_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1ON :1;
		uint16_t u8PWM :8;
	};
} Tst_eXS_PWMR_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1OSdis :1;
		uint16_t u1OLONdis :1;
		uint16_t u1OLOFFdis :1;
		uint16_t u1DISdis :1;
		uint16_t u2SR :2;
		uint16_t u3DELAY :3;
	};
} Tst_eXS_CONFR_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1HOCR :1;
		uint16_t u1PR :1;
		uint16_t u1CLOCKsel :1;
		uint16_t u1CSNSrat :1;
		uint16_t u1tOCH :1;
		uint16_t u1tOCM :1;
		uint16_t u1OCH :1;
		uint16_t u1OCM :1;
		uint16_t u1OCL :1;
	};
} Tst_eXS_OCR_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t :1;
		uint16_t u4RetryCount :4;
		uint16_t u2Autoperiod :2;
		uint16_t u1Retryinlim :1;
		uint16_t u1Retry_s :1;
	};
} Tst_eXS_RETRYR_Out_Bits;


typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1PWM1en :1;
		uint16_t u1PWM0en :1;
		uint16_t u1PARALLEL :1;
		uint16_t u1THen :1;
		uint16_t u1WDdis :1;
		uint16_t u1VDDFAILen :1;
		uint16_t u1CSNS1 :1;
		uint16_t u1CSNS0 :1;
		uint16_t u1OVdis :1;
	};
} Tst_eXS_GCR_Out_Bits;

typedef volatile union {
	uint16_t All;
	struct {
		uint16_t u1WDIN :1;
		uint16_t u1PF :1;
		uint16_t u1A0 :1;
		uint16_t u3SOA :3;
		uint16_t u1NM :1;
		uint16_t u1CONF1 :1;
		uint16_t u1CONF0 :1;
		uint16_t u1ID1 :1;
		uint16_t u1ID0 :1;
		uint16_t u1IN1 :1;
		uint16_t u1IN0 :1;
		uint16_t u1CLOCKfail :1;
		uint16_t u1CALfail1 :1;
		uint16_t u1CALfail0 :1;
	};
} Tst_eXS_DIAGR_Out_Bits;

typedef struct {
	uint16_t u1WD :1;
	uint16_t u1P :1;
	uint16_t u1Ch :1;
	uint16_t u3 :3;
	uint16_t :6;
	uint16_t u3SOA :3;

} StartReg;

typedef union
{
	uint16_t XS_Spd_In_Data[7];
	struct
	{
		Tst_eXS_STATR_In_Bits 	XS_StartIn; //Start register
		Tst_eXS_PWMR_In_Bits 	XS_PwmIn;	//pwm register
		Tst_eXS_CONFR_In_Bits 	XS_ConfIn;	//configuration register
		Tst_eXS_OCR_In_Bits 	XS_OcrIn;	//ocr register
		Tst_eXS_RETRYR_In_Bits 	XS_RetrayIn;//Retry register
		Tst_eXS_GCR_In_Bits 	XS_GcrIn;	//GCR register
		uint16_t			   	XS_CalbIn;	//Calibration register
	};
} XS_SpdInRegsData;

typedef union
{
	uint16_t XS_Spd_Out_Data[8];
	struct
	{
		Tst_eXS_STATR_Out_Bits 	XS_StartOut; //Start register
		Tst_eXS_eFAULT_Out_Bits XS_FaultOut; // Fault register
		Tst_eXS_PWMR_Out_Bits 	XS_PwmOut;	 //pwm register
		Tst_eXS_CONFR_Out_Bits 	XS_ConfOut;	 //configuration register
		Tst_eXS_OCR_Out_Bits 	XS_OcrOut;	 //ocr register
		Tst_eXS_RETRYR_Out_Bits XS_RetrayOut;//Retry register
		Tst_eXS_GCR_Out_Bits 	XS_GcrOut;	 //GCR register
		Tst_eXS_DIAGR_Out_Bits	XS_DiagOut;	 //Calibration register
	};
} XS_SpdOutRegsData;

typedef XS_SpdOutRegsData* XS_SpdOutRegsDataPtr;
typedef XS_SpdInRegsData* XS_SpdInRegsDataPtr;
/** MC10XS3412 initial default configuration */
uint16_t u16DrvDes_MC20XS4200_Config(XS_SpdInRegsDataPtr XS_SpdInRegsData);

/* update pwm */
uint16_t u16Drvdes_MC20XS4200_PwmUpdate(XS_SpdInRegsDataPtr XS_SpdInRegsData, uint16_t duty);

/** Outputs configuration */
void des_MC20XS3400_HS_Configuration(uint8_t u8HSout, uint8_t u8Dir_control,
		uint8_t u8SlewRate, uint8_t u8SwitchDelay);
/** Outputs control */
void des_MC20XS34200_HS_Control(uint8_t u8HSout, uint8_t u8HS_State,
		uint8_t u8HS_PWM);

/** PWM module selection */
void des_MC20XS3400_Select_PWM_Module(uint8_t u8PWMSelect);

/** Select current sense ratio */
void des_MC20XS3400_Select_CurrentSense(uint8_t u8HSout, uint8_t u8SenseRatio);

/** Select current profile */
void des_MC20XS3400_Select_CurrentProfile(uint8_t u8HSout, uint8_t u8Xenon,
		uint8_t u8CoolCurve, uint8_t u8InrushCurve);
/** Select open load detection */
void des_MC20XS3400_OpenLoad_Detect(uint8_t u8HSout, uint8_t u8OLselect);

/** Select output steady state */
void des_MC20XS3400_Select_SteadyState(uint8_t u8HSout, uint8_t u8SteadyState);

/** Select over-current mode */
void des_MC20XS3400_Select_OverCurrent(uint8_t u8HSout, uint8_t u8OCmode);

/** Read HS output status */
uint8_t des_MC20XS3400_Get_Status(uint8_t u8HSout);

/* Frequency calibration */
void des_MC20XS4200_Calib_Freq(void);

/* task called each 10 ms */
void des_MC20XS4200_Task(void);


uint16_t u16Drvdes_XS_iParityCalc (uint16_t Fu16Word);
#endif /* INC_MC20XS4200_DRV_H_ */
