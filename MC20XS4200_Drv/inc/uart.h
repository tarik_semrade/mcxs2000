/*
 * uart.h
 *
 *  Created on: 18 oct. 2021
 *      Author: STuser
 */

#ifndef INC_UART_H_
#define INC_UART_H_

void vUart_Send_Data(uint16_t *data);
void vUart_Send_String(uint16_t *String, uint16_t size);
void vdecToBinary(int16_t n);
int16_t uint16_to_bin(int16_t s16Decimal);

#endif /* INC_UART_H_ */
