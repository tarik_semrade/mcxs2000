/*
 * Copyright 2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : pit_periodic_interrupt_mpc5748g
**     Processor   : MPC5748G_324
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-03-15, 12:21, # CodeGen: 6
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */
/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pit1.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "dspi1.h"
#include "uart_pal.h"
#include "uart.h"

#include "MC20XS4200_Drv.h"
#include "pins_driver.h"
volatile int exit_code = 0;

#define BUFFER_SIZE 14U
#define NUMBER_OF_FRAMES 7U
#define TIMEOUT 1000U
//PWM_Register  = ((Duty * 255)/100);
#define  Mu8XS_iCalcDuty(u16Duty)      ((uint8_t) ((255 * (u16Duty)) / 100 ))

#define welcomeMsg "This example is an simple echo using UART_PAL\r\n\
it will send back any character you send to it.\r\n\
The board will greet you if you send 'Hello!'\r\
\nNow you can begin typing:\r\n"
/* User includes (#include below this line is not maintained by Processor Expert) */
/* This example is setup to work by default with DEVKIT. To use it with other boards
 please comment the following line
*/


/**
 * PE[5] = FSB   D5
 * PE[1] = RST   D6
 * PE[0] = FS0B  D7
 */
/* Write your local variable definition here */

XS_SpdInRegsData master_send[2] = {0};
uint16_t master_send_cache[7 * 2];
status_t Dspi_status = STATUS_SUCCESS;
dspi_transfer_status_t DSp_Busy;
//uint8_t master_receive[BUFFER_SIZE];
uint16_t count;
uint8_t master_receive[BUFFER_SIZE];
uint8_t slave_send[] = { 1, 2, 3, 4, 5, 6, 7 };
uint8_t slave_receive[BUFFER_SIZE];
uint16_t count1 = 0;
uint16_t count2 = 0;
uint16_t DutyCycle = 100;
uint16_t *Pointer = NULL;
uint16_t delay = 10U;

uint16_t Ticks = 0;


uint16_t u16FSBPort = 0U;
uint16_t FSB;
uint16_t FSOB;
uint16_t u16Reset_switch = 1U;

uint16_t u16Status = 0U;
uint16_t u16ManageReset = 0U;

#define  DEVKIT
#ifdef DEVKIT
	#define PORT PTA
	#define LED 10       /* pin PA[10] - LED3 (DS4) on DEV-KIT */
#else
	#define PORT PTG
	#define LED 2        /* pin PG[2] - LED1 (DS2) on Motherboard */
#endif
/**
 *
 */
void PIT_Ch0_IRQHandler(void)
{
	/* Toggle led */
	PINS_DRV_TogglePins(PORT, (1 << LED));

	/* Clear channel 0 interrupt flag */
	PIT_DRV_ClearStatusFlags(INST_PIT1, pit1_ChnConfig0.hwChannel);

	/* Activate the 10ms tick */
	Ticks = 1U;

}

/*! 
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
	/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
	#ifdef PEX_RTOS_INIT
	PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
	#endif
	/*** End of Processor Expert internal initialization.                    ***/
	/* Initialize clock gate*/
	CLOCK_SYS_Init(g_clockManConfigsArr,   CLOCK_MANAGER_CONFIG_CNT,  g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
	CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

	/* Initialize and configure pins */
	PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

	/*
	 * Init the DSPI peripheral*/
	/* SPI master configuration: clock speed: 1 Mhz, 16 bits/frame, MSB first */
	DSPI_MasterInit(DSPI0_INSTANCE, &dspi1State, &dspi1_MasterInitConfig0);

	/* Configure delay between transfer, delay between SCK and PCS and delay between PCS and SCK */
	DSPI_MasterSetDelay(DSPI0_INSTANCE, 1, 1, 1);

	/*
	 *
	 * Init the PIT peripheral
	/* Initialize PIT */
	PIT_DRV_Init(INST_PIT1, &pit1_InitConfig);

	/* Initialize channel 0 */
	PIT_DRV_InitChannel(INST_PIT1, &pit1_ChnConfig0);

	/* Start channel 0 counting */
	PIT_DRV_StartChannel(INST_PIT1, pit1_ChnConfig0.hwChannel);

	/* Initialize UART PAL over LINFlexD */
	UART_Init(&uart_pal1_instance, &uart_pal1_Config0);

	/* Send a welcome message */
	UART_SendDataBlocking(&uart_pal1_instance, (uint8_t *) welcomeMsg,strlen(welcomeMsg), TIMEOUT);

	/* Init */
	DutyCycle = 100;


	/* Initialize the master send buffer */
	u16Status = u16DrvDes_MC20XS4200_Config(&master_send[0]);

	/* The infinite while loop */
	while (1)
	{
		if (Ticks == 1U)
		{
			/* RESET  = 1 */
			if (u16Reset_switch)
			{
				PINS_DRV_SetPins(PTE, 1 << 1);
			}
			else
			{
				PINS_DRV_ClearPins(PTE, 1 << 1);
			}

			//Read PORT E
			u16FSBPort = PINS_DRV_ReadPins(PTE);

			//Read FSB and FSOB
			FSB = (uint16_t) ((u16FSBPort & 0x0010) >> 5);
			FSOB = (uint16_t) ((u16FSBPort & 0x0001) >> 0);

			/* Manage reset sequence */
			if (u16ManageReset == 0)
			{
				/* reconfigure if reset */
				u16Status = u16DrvDes_MC20XS4200_Config(&master_send[0]);

				/* Disable reset */
				u16ManageReset = 1U;
			}

			/* update the the period */
			u16Drvdes_MC20XS4200_PwmUpdate(&master_send[0], Mu8XS_iCalcDuty(DutyCycle));

		}

		/* 10 ms tick disable */
		Ticks = 0;
	}

	/*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/
/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP C55 series of microcontrollers.
**
** ###################################################################
*/
