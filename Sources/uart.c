/*
 * uart.c
 *
 *  Created on: 18 oct. 2021
 *      Author: STuser
 */

#include "uart_pal1.h"
#include "uart_pal.h"
#define TIMEOUT 1000U
/***
 *
 */
int16_t uint16_to_bin(int16_t s16Decimal)
{
   char *bin;
   int16_t s16Bin;

   bin = calloc(1, 1);
   while (s16Decimal > 0)
   {
      bin = realloc(bin, strlen(bin) + 2);
      bin[strlen(bin) - 1] = (s16Decimal % 2) + '0';
      bin[strlen(bin)] = '\0';
      s16Decimal = s16Decimal / 2;
   }
   s16Bin = atoi(bin);
   free(bin);
   return s16Bin;
}


/**
 * function to convert decimal to binary
 */
void vdecToBinary(int16_t n)
{
    /* Array to store binary number */
    int16_t binaryNum[32];

    /* Counter for binary array */
    int16_t i = 0;
    while (n > 0)
    {
        /* storing remainder in binary array */
        binaryNum[i] = n % 2;
        n = n / 2;
        i++;
    }

}

void vUart_Send_Data(uint16_t *data)
{
	uint8_t *Prt1;
	uint8_t *Prt2;

	*Prt1 = (uint8_t)(*data && 0x00FF);
	*Prt2 = (uint8_t)((*data && 0xFF00) >> 8);

	if ((NULL != Prt1) && (NULL != Prt2))
	{
		/* Send the first part */
		UART_SendDataBlocking(&uart_pal1_instance, (uint8_t *) Prt1, strlen(Prt1), TIMEOUT);

		/* Send the second pat */
		UART_SendDataBlocking(&uart_pal1_instance, (uint8_t *) Prt2, strlen(Prt2), TIMEOUT);
	}

}
void vUart_Send_String(uint16_t *String, uint16_t size)
{
	uint16_t Indxer= 0u;


	for (Indxer = 0; Indxer < size; ++Indxer)
	{
		vUart_Send_Data(String);
	}
}
